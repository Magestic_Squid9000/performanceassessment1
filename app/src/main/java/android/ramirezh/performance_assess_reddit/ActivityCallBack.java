package android.ramirezh.performance_assess_reddit;

import android.net.Uri;

public interface ActivityCallBack {
    void onPostSelected(Uri redditPostUri);
}
